import AcademicCapIcon from './AcademicCapIcon';
import AdjustmentsIcon from './AdjustmentsIcon';
import AnnotationIcon from './AnnotationIcon';
import ArchiveIcon from './ArchiveIcon';
import ArrowDownIcon from './ArrowDownIcon';
import ArrowLeftIcon from './ArrowLeftIcon';
import ArrowRightIcon from './ArrowRightIcon';
import ArrowUpIcon from './ArrowUpIcon';
import BellIcon from './BellIcon';
import BellOffIcon from './BellOffIcon';
import BoldIcon from './BoldIcon';
import BookmarkIcon from './BookmarkIcon';
import BriefcaseAltIcon from './BriefcaseAltIcon';
import BriefcaseIcon from './BriefcaseIcon';
import BulbIcon from './BulbIcon';
import CalendarIcon from './CalendarIcon';
import CameraIcon from './CameraIcon';
import CashIcon from './CashIcon';
import ChartSquareBarIcon from './ChartSquareBarIcon';
import ChatAlt2Icon from './ChatAlt2Icon';
import ChatAltIcon from './ChatAltIcon';
import ChatIcon from './ChatIcon';
import CheckCircleIcon from './CheckCircleIcon';
import CheckIcon from './CheckIcon';
import ChevronDoubleUpIcon from './ChevronDoubleUpIcon';
import ChevronDownIcon from './ChevronDownIcon';
import ChevronLeftIcon from './ChevronLeftIcon';
import ChevronRightIcon from './ChevronRightIcon';
import ChevronUpIcon from './ChevronUpIcon';
import ChipIcon from './ChipIcon';
import CircleIcon from './CircleIcon';
import ClipboardIcon from './ClipboardIcon';
import ClipboardListIcon from './ClipboardListIcon';
import CodeIcon from './CodeIcon';
import CodeBlockIcon from './CodeBlockIcon';
import CollectionIcon from './CollectionIcon';
import CreditCardIcon from './CreditCardIcon';
import CrownIcon from './CrownIcon';
import CurrencyDollarIcon from './CurrencyDollarIcon';
import CursorClickIcon from './CursorClickIcon';
import DatabaseIcon from './DatabaseIcon';
import DesktopComputerIcon from './DesktopComputerIcon';
import DocumentTextIcon from './DocumentTextIcon';
import DotsHorizontalIcon from './DotsHorizontalIcon';
import DotsVerticalIcon from './DotsVerticalIcon';
import DownloadIcon from './DownloadIcon';
import DragHandleIcon from './DragHandle';
import ExclamationTriangleIcon from './ExclamationTriangleIcon';
import ExclamationCircleIcon from './ExclamationCircleIcon';
import ExploreIcon from './ExploreIcon';
import ExternalLinkIcon from './ExternalLinkIcon';
import EyeIcon from './EyeIcon';
import FilmIcon from './FilmIcon';
import FireIcon from './FireIcon';
import FlagIcon from './FlagIcon';
import FolderIcon from './FolderIcon';
import GiftIcon from './GiftIcon';
import GraphIcon from './GraphIcon';
import HamburgerIcon from './HamburgerIcon';
import HashtagIcon from './HashtagIcon';
import HeartIcon from './HeartIcon';
import HighlightAddIcon from './HighlightAddIcon';
import HighlightIcon from './HighlightIcon';
import HistoryIcon from './HistoryIcon';
import HomeIcon from './HomeIcon';
import InformationCircleIcon from './InformationCircleIcon';
import ItalicIcon from './ItalicIcon';
import KeyboardHideIcon from './KeyboardHideIcon';
import LinkIcon from './LinkIcon';
import LightningBoltIcon from './LightningBoltIcon';
import ListBulletedIcon from './ListBulletedIcon';
import ListNumberedIcon from './ListNumberedIcon';
import LockClosedIcon from './LockClosedIcon';
import LockOpenIcon from './LockOpenIcon';
import LogoutIcon from './LogoutIcon';
import MailIcon from './MailIcon';
import MapPinIcon from './MapPinIcon';
import MinusCircleIcon from './MinusCircleIcon';
import MinusIcon from './MinusIcon';
import MyContentsIcon from './MyContentsIcon';
import NewspaperIcon from './NewspaperIcon';
import PaperClipIcon from './PaperClipIcon';
import PencilAlt2Icon from './PencilAlt2Icon';
import PencilAltIcon from './PencilAltIcon';
import PencilIcon from './PencilIcon';
import PhoneIcon from './PhoneIcon';
import PhotographIcon from './PhotographIcon';
import PlusBoxIcon from './PlusBoxIcon';
import PlusChatIcon from './PlusChatIcon';
import PlusCircleIcon from './PlusCircleIcon';
import PlusIcon from './PlusIcon';
import QuestionMarkCircleIcon from './QuestionMarkCircleIcon';
import PollAltIcon from './PollAltIcon';
import PollAlt2Icon from './PollAlt2Icon';
import PollIcon from './PollIcon';
import ReloadAltIcon from './ReloadAltIcon';
import ReloadIcon from './ReloadIcon';
import ReplyIcon from './ReplyIcon';
import RepostIcon from './RepostIcon';
import RobotIcon from './RobotIcon';
import SearchIcon from './SearchIcon';
import SendIcon from './SendIcon';
import ServerIcon from './ServerIcon';
import SettingIcon from './SettingIcon';
import ShareIcon from './ShareIcon';
import SpeakerphoneIcon from './SpeakerphoneIcon';
import StarIcon from './StarIcon';
import StarAltIcon from './StarAltIcon';
import StrikethroughIcon from './StrikethroughIcon';
import TagIcon from './TagIcon';
import ThumbUpIcon from './ThumbUpIcon';
import ThumbDownIcon from './ThumbDownIcon';
import TocIcon from './TocIcon';
import TrashIcon from './TrashIcon';
import TrophyIcon from './TrophyIcon';
import UnderlineIcon from './UnderlineIcon';
import UserAddIcon from './UserAddIcon';
import UserCircleIcon from './UserCircleIcon';
import UserGroupIcon from './UserGroupIcon';
import UserIcon from './UserIcon';
import UserRemoveIcon from './UserRemoveIcon';
import UsersIcon from './UsersIcon';
import VerifiedIcon from './VerifiedIcon';
import VideoCameraIcon from './VideoCameraIcon';
import XBoxIcon from './XBoxIcon';
import XCircleIcon from './XCircleIcon';
import XIcon from './XIcon';
import OfficeBuildingIcon from './OfficeBuildingIcon';
import PushPinIcon from './PushPinIcon';
import CarrieTabIcon from './CarrieTabIcon';
import CarrieIcon from './CarrieIcon';

export {
  AcademicCapIcon,
  AdjustmentsIcon,
  AnnotationIcon,
  ArchiveIcon,
  ArrowDownIcon,
  ArrowLeftIcon,
  ArrowRightIcon,
  ArrowUpIcon,
  BellOffIcon,
  BellIcon,
  BoldIcon,
  BookmarkIcon,
  BriefcaseAltIcon,
  BriefcaseIcon,
  BulbIcon,
  CalendarIcon,
  CameraIcon,
  CashIcon,
  ChartSquareBarIcon,
  ChatAlt2Icon,
  ChatAltIcon,
  ChatIcon,
  CheckCircleIcon,
  CheckIcon,
  ChevronDoubleUpIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ChevronUpIcon,
  ChipIcon,
  CircleIcon,
  ClipboardIcon,
  ClipboardListIcon,
  CodeIcon,
  CodeBlockIcon,
  CollectionIcon,
  CreditCardIcon,
  CrownIcon,
  CurrencyDollarIcon,
  CursorClickIcon,
  DatabaseIcon,
  DesktopComputerIcon,
  DocumentTextIcon,
  DotsHorizontalIcon,
  DotsVerticalIcon,
  DownloadIcon,
  DragHandleIcon,
  ExclamationTriangleIcon,
  ExclamationCircleIcon,
  ExploreIcon,
  ExternalLinkIcon,
  EyeIcon,
  FilmIcon,
  FireIcon,
  FolderIcon,
  FlagIcon,
  GiftIcon,
  GraphIcon,
  HamburgerIcon,
  HashtagIcon,
  HeartIcon,
  HighlightAddIcon,
  HighlightIcon,
  HistoryIcon,
  HomeIcon,
  InformationCircleIcon,
  ItalicIcon,
  KeyboardHideIcon,
  LightningBoltIcon,
  ListBulletedIcon,
  ListNumberedIcon,
  LinkIcon,
  LockClosedIcon,
  LockOpenIcon,
  LogoutIcon,
  MailIcon,
  MapPinIcon,
  MinusCircleIcon,
  MinusIcon,
  MyContentsIcon,
  NewspaperIcon,
  PaperClipIcon,
  PencilAlt2Icon,
  PencilAltIcon,
  PencilIcon,
  PhoneIcon,
  PhotographIcon,
  PlusBoxIcon,
  PlusChatIcon,
  PlusCircleIcon,
  PlusIcon,
  PollAltIcon,
  PollAlt2Icon,
  PollIcon,
  QuestionMarkCircleIcon,
  ReloadAltIcon,
  ReloadIcon,
  ReplyIcon,
  RepostIcon,
  RobotIcon,
  SearchIcon,
  SendIcon,
  ServerIcon,
  SettingIcon,
  ShareIcon,
  SpeakerphoneIcon,
  StarIcon,
  StarAltIcon,
  StrikethroughIcon,
  TagIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  TocIcon,
  TrashIcon,
  TrophyIcon,
  UnderlineIcon,
  UserAddIcon,
  UserCircleIcon,
  UserGroupIcon,
  UserIcon,
  UserRemoveIcon,
  UsersIcon,
  VerifiedIcon,
  VideoCameraIcon,
  XBoxIcon,
  XCircleIcon,
  XIcon,
  OfficeBuildingIcon,
  PushPinIcon,
  CarrieTabIcon,
  CarrieIcon,
};
