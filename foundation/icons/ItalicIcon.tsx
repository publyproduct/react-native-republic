import { Variables } from '..';

function ItalicIcon(
  params: {
    type?: 'outline' | 'solid';
    width?: number;
    height?: number;
    fill?: string;
    stroke?: string;
    strokeWidth?: number;
  } = {},
) {
  const fillKey = params.fill ?? 'black'
  const fillValue = Variables.themeColors[fillKey];
  const strokeValue = Variables.themeColors[params.stroke ?? fillKey];
  if (params.type === 'solid') {
    return `
      <svg 
        width="${params.width ?? 24}" 
        height="${params.height ?? 24}" 
        fill="${fillValue}"  
        stroke="${strokeValue}" 
        strokeWidth="${params.strokeWidth ?? 0}"
        viewBox="0 0 24 24"
      >
        <path d="M10 5.5C10 6.33 10.67 7 11.5 7H12.21L8.79 15H7.5C6.67 15 6 15.67 6 16.5C6 17.33 6.67 18 7.5 18H12.5C13.33 18 14 17.33 14 16.5C14 15.67 13.33 15 12.5 15H11.79L15.21 7H16.5C17.33 7 18 6.33 18 5.5C18 4.67 17.33 4 16.5 4H11.5C10.67 4 10 4.67 10 5.5Z"/>
      </svg>
    `;
  }
  return `
    <svg 
      width="${params.width ?? 24}" 
      height="${params.height ?? 24}" 
      fill="${fillValue}"  
      stroke="${strokeValue}" 
      strokeWidth="${params.strokeWidth ?? 0}"
      viewBox="0 0 24 24"
    >
      <path d="M10 5.5C10 6.33 10.67 7 11.5 7H12.21L8.79 15H7.5C6.67 15 6 15.67 6 16.5C6 17.33 6.67 18 7.5 18H12.5C13.33 18 14 17.33 14 16.5C14 15.67 13.33 15 12.5 15H11.79L15.21 7H16.5C17.33 7 18 6.33 18 5.5C18 4.67 17.33 4 16.5 4H11.5C10.67 4 10 4.67 10 5.5Z"/>
    </svg>
  `;
}

export default ItalicIcon;
