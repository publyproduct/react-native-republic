import { Variables } from '..';

function KeyboardHideIcon(
  params: {
    type?: 'outline' | 'solid';
    width?: number;
    height?: number;
    fill?: string;
    stroke?: string;
    strokeWidth?: number;
  } = {},
) {
  const fillKey = params.fill ?? 'black'
  const fillValue = Variables.themeColors[fillKey];
  const strokeValue = Variables.themeColors[params.stroke ?? fillKey];
  if (params.type === 'solid') {
    return `
      <svg 
        width="${params.width ?? 24}" 
        height="${params.height ?? 24}" 
        fill="${fillValue}"  
        stroke="${strokeValue}" 
        strokeWidth="${params.strokeWidth ?? 0}"
        viewBox="0 0 24 24"
      >
        <path d="M12 23L8 19H16L12 23ZM4 17C3.45 17 2.97917 16.8042 2.5875 16.4125C2.19583 16.0208 2 15.55 2 15V5C2 4.45 2.19583 3.97917 2.5875 3.5875C2.97917 3.19583 3.45 3 4 3H20C20.55 3 21.0208 3.19583 21.4125 3.5875C21.8042 3.97917 22 4.45 22 5V15C22 15.55 21.8042 16.0208 21.4125 16.4125C21.0208 16.8042 20.55 17 20 17H4ZM4 15H20V5H4V15ZM8 14H16V12H8V14ZM5 11H7V9H5V11ZM8 11H10V9H8V11ZM11 11H13V9H11V11ZM14 11H16V9H14V11ZM17 11H19V9H17V11ZM5 8H7V6H5V8ZM8 8H10V6H8V8ZM11 8H13V6H11V8ZM14 8H16V6H14V8ZM17 8H19V6H17V8Z"/>
      </svg>
    `;
  }
  return `
    <svg 
      width="${params.width ?? 24}" 
      height="${params.height ?? 24}" 
      fill="${fillValue}"  
      stroke="${strokeValue}" 
      strokeWidth="${params.strokeWidth ?? 0}"
      viewBox="0 0 24 24"
    >
      <path d="M12 23L8 19H16L12 23ZM4 17C3.45 17 2.97917 16.8042 2.5875 16.4125C2.19583 16.0208 2 15.55 2 15V5C2 4.45 2.19583 3.97917 2.5875 3.5875C2.97917 3.19583 3.45 3 4 3H20C20.55 3 21.0208 3.19583 21.4125 3.5875C21.8042 3.97917 22 4.45 22 5V15C22 15.55 21.8042 16.0208 21.4125 16.4125C21.0208 16.8042 20.55 17 20 17H4ZM4 15H20V5H4V15ZM8 14H16V12H8V14ZM5 11H7V9H5V11ZM8 11H10V9H8V11ZM11 11H13V9H11V11ZM14 11H16V9H14V11ZM17 11H19V9H17V11ZM5 8H7V6H5V8ZM8 8H10V6H8V8ZM11 8H13V6H11V8ZM14 8H16V6H14V8ZM17 8H19V6H17V8Z"/>
    </svg>
  `;
}

export default KeyboardHideIcon;
