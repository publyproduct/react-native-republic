import { StyleSheet } from 'react-native';

const opacity = StyleSheet.create({
  opacity0: { opacity: 0 },
  opacity10: { opacity: .1 },
  opacity20: { opacity: .2 },
  opacity30: { opacity: .3 },
  opacity40: { opacity: .4 },
  opacity50: { opacity: .5 },
  opacity60: { opacity: .6 },
  opacity70: { opacity: .7 },
  opacity80: { opacity: .8 },
  opacity90: { opacity: .9 },
  opacity100: { opacity: 1 },
});

export default { ...opacity };