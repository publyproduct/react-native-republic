import { StyleSheet } from 'react-native';

// todo: background opacity(alpha) 클래스 추가해야 함

const bgOpacity = StyleSheet.create({
  coral: { backgroundColor: 'rgba(255, 237, 238, 0.5)' },
});

export default bgOpacity;