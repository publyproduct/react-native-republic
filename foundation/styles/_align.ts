import { StyleSheet } from 'react-native';

const align = StyleSheet.create({
  auto: { textAlignVertical: 'auto' },
  top: { textAlignVertical: 'top' },
  bottom: { textAlignVertical: 'bottom' },
  center: { textAlignVertical: 'center' },
});

export default align;
