export default class Variables {
  // Color system
  static black = '#000000';
  static white = '#ffffff';

  static slate50 = '#F8FAFC';
  static slate100 = '#F1F5F9';
  static slate200 = '#E2E8F0';
  static slate300 = '#CBD5E1';
  static slate400 = '#94A3B8';
  static slate500 = '#64748B';
  static slate600 = '#475569';
  static slate700 = '#334155';
  static slate800 = '#1E293B';
  static slate900 = '#0F172A';

  static gray100 = '#F8FAFC';
  // 현재 gray100에 slate50값 적용되어있음
  static gray200 = '#E2E8F0';
  static gray300 = '#CBD5E1';
  static gray400 = '#CBD5E1';
  // 현재 gray400에 slate300값 적용되어있음
  static gray500 = '#94A3B8';
  // 현재 gray500에 slate400값 적용되어있음
  static gray600 = '#94A3B8';
  // 현재 gray600에 slate400값 적용되어있음
  static gray700 = '#64748B';
  // 현재 gray700에 slate500값 적용되어있음
  static gray800 = '#475569';
  // 현재 gray800에 slate600값 적용되어있음
  static gray900 = '#0F172A';

  static coral50 = '#FDF4F3';
  static coral100 = '#ffedee';
  static coral200 = '#FDD1CB';
  static coral300 = '#F7B4AA';
  static coral400 = '#f39b8e';
  static coral500 = '#ef7b6a';
  static coral600 = '#ed6653';
  static coral700 = '#e1432e';
  static coral800 = '#be1e08';
  static coral900 = '#7A2D22';

  static teal50 = '#F0FDFA';
  static teal100 = '#CCFBF1';
  static teal200 = '#99F6E4';
  static teal300 = '#5EEAD4';
  static teal400 = '#2DD4Bf';
  static teal500 = '#14B8A6';
  static teal600 = '#0D9488';
  static teal700 = '#0F766E';
  static teal800 = '#115E59';
  static teal900 = '#134E4A';

  static red50 = '#FEF2F2';
  static red100 = '#FEE2E2';
  static red200 = '#FECACA';
  static red300 = '#FCA5A5';
  static red400 = '#F87171';
  static red500 = '#EF4444';
  static red600 = '#DC2626';
  static red700 = '#B91C1C';
  static red800 = '#991B1B';
  static red900 = '#7F1D1D';

  static green50 = '#F0FDF4';
  static green100 = '#DCFCE7';
  static green200 = '#BBF7D0';
  static green300 = '#86EFAC';
  static green400 = '#4ADE80';
  static green500 = '#22C55E';
  static green600 = '#16A34A';
  static green700 = '#15803D';
  static green800 = '#166534';
  static green900 = '#14532D';

  static yellow50 = '#FEFCE8';
  static yellow100 = '#FEF9C3';
  static yellow200 = '#FEF08A';
  static yellow300 = '#FDE047';
  static yellow400 = '#FACC15';
  static yellow500 = '#EAB308';
  static yellow600 = '#CA8A04';
  static yellow700 = '#A16207';
  static yellow800 = '#854D0E';
  static yellow900 = '#713F12';

  static blue50 = '#EFF6FF';
  static blue100 = '#DBEAFE';
  static blue200 = '#BFDBFE';
  static blue300 = '#93C5FD';
  static blue400 = '#60A5FA';
  static blue500 = '#3B82F6';
  static blue600 = '#2563EB';
  static blue700 = '#1D4ED8';
  static blue800 = '#1E40AF';
  static blue900 = '#1E3A8A';

  static indigo50 = '#EEF2FF';
  static indigo100 = '#E0E7FF';
  static indigo200 = '#C7D2FE';
  static indigo300 = '#A5B4FC';
  static indigo400 = '#818CF8';
  static indigo500 = '#6366F1';
  static indigo600 = '#4F46E5';
  static indigo700 = '#4338CA';
  static indigo800 = '#3730A3';
  static indigo900 = '#312E81';

  static purple50 = '#FAF5FF';
  static purple100 = '#F3E8FF';
  static purple200 = '#E9D5FF';
  static purple300 = '#D8B4FE';
  static purple400 = '#C084FC';
  static purple500 = '#A855F7';
  static purple600 = '#9333EA';
  static purple700 = '#7E22CE';
  static purple800 = '#6B21A8';
  static purple900 = '#581C87';

  static magenta50 = '#FDF4FF';
  static magenta100 = '#FAE8FF';
  static magenta200 = '#F5D0FE';
  static magenta300 = '#F0ABFC';
  static magenta400 = '#E879F9';
  static magenta500 = '#D946EF';
  static magenta600 = '#C026D3';
  static magenta700 = '#A21CAF';
  static magenta800 = '#86198F';
  static magenta900 = '#701A75';

  static themeColors = {
    black: Variables.black,
    white: Variables.white,
    slate50: Variables.slate50,
    slate100: Variables.slate100,
    slate200: Variables.slate200,
    slate300: Variables.slate300,
    slate400: Variables.slate400,
    slate500: Variables.slate500,
    slate600: Variables.slate600,
    slate700: Variables.slate700,
    slate800: Variables.slate800,
    slate900: Variables.slate900,
    gray100: Variables.gray100,
    gray200: Variables.gray200,
    gray300: Variables.gray300,
    gray400: Variables.gray400,
    gray500: Variables.gray500,
    gray600: Variables.gray600,
    gray700: Variables.gray700,
    gray800: Variables.gray800,
    gray900: Variables.gray900,
    coral50: Variables.coral50,
    coral100: Variables.coral100,
    coral200: Variables.coral200,
    coral300: Variables.coral300,
    coral400: Variables.coral400,
    coral500: Variables.coral500,
    coral600: Variables.coral600,
    coral700: Variables.coral700,
    coral800: Variables.coral800,
    coral900: Variables.coral900,
    teal50: Variables.teal50,
    teal100: Variables.teal100,
    teal200: Variables.teal200,
    teal300: Variables.teal300,
    teal400: Variables.teal400,
    teal500: Variables.teal500,
    teal600: Variables.teal600,
    teal700: Variables.teal700,
    teal800: Variables.teal800,
    teal900: Variables.teal900,
    red50: Variables.red50,
    red100: Variables.red100,
    red200: Variables.red200,
    red300: Variables.red300,
    red400: Variables.red400,
    red500: Variables.red500,
    red600: Variables.red600,
    red700: Variables.red700,
    red800: Variables.red800,
    red900: Variables.red900,
    green50: Variables.green50,
    green100: Variables.green100,
    green200: Variables.green200,
    green300: Variables.green300,
    green400: Variables.green400,
    green500: Variables.green500,
    green600: Variables.green600,
    green700: Variables.green700,
    green800: Variables.green800,
    green900: Variables.green900,
    yellow50: Variables.yellow50,
    yellow100: Variables.yellow100,
    yellow200: Variables.yellow200,
    yellow300: Variables.yellow300,
    yellow400: Variables.yellow400,
    yellow500: Variables.yellow500,
    yellow600: Variables.yellow600,
    yellow700: Variables.yellow700,
    yellow800: Variables.yellow800,
    yellow900: Variables.yellow900,
    blue50: Variables.blue50,
    blue100: Variables.blue100,
    blue200: Variables.blue200,
    blue300: Variables.blue300,
    blue400: Variables.blue400,
    blue500: Variables.blue500,
    blue600: Variables.blue600,
    blue700: Variables.blue700,
    blue800: Variables.blue800,
    blue900: Variables.blue900,
    indigo50: Variables.indigo50,
    indigo100: Variables.indigo100,
    indigo200: Variables.indigo200,
    indigo300: Variables.indigo300,
    indigo400: Variables.indigo400,
    indigo500: Variables.indigo500,
    indigo600: Variables.indigo600,
    indigo700: Variables.indigo700,
    indigo800: Variables.indigo800,
    indigo900: Variables.indigo900,
    purple50: Variables.purple50,
    purple100: Variables.purple100,
    purple200: Variables.purple200,
    purple300: Variables.purple300,
    purple400: Variables.purple400,
    purple500: Variables.purple500,
    purple600: Variables.purple600,
    purple700: Variables.purple700,
    purple800: Variables.purple800,
    purple900: Variables.purple900,
    magenta50: Variables.magenta50,
    magenta100: Variables.magenta100,
    magenta200: Variables.magenta200,
    magenta300: Variables.magenta300,
    magenta400: Variables.magenta400,
    magenta500: Variables.magenta500,
    magenta600: Variables.magenta600,
    magenta700: Variables.magenta700,
    magenta800: Variables.magenta800,
    magenta900: Variables.magenta900,
  };

  // Spacing
  static spacer = 16;
  static spacers = {
    '0': 0,
    '1': Variables.spacer * 0.25,
    '2': Variables.spacer * 0.5,
    '3': Variables.spacer * 0.75,
    '4': Variables.spacer,
    '5': Variables.spacer * 1.25,
    '6': Variables.spacer * 1.5,
    '7': Variables.spacer * 1.75,
    '8': Variables.spacer * 2,
    '9': Variables.spacer * 2.25,
  };

  // Sizing
  static sizes = {
    '10': '10%',
    '15': '15%',
    '25': '25%',
    '31': '31.6%',
    '33': '33.3%',
    '35': '35%',
    '50': '50%',
    '66': '66.7%',
    '75': '75%',
    '100': '100%',
    auto: 'auto',
  };

  static fontSizes = {
    fontSize5XLarge: 3,
    fontSize4XLarge: 2.25,
    fontSize3XLarge: 1.875,
    fontSize2XLarge: 1.5,
    fontSizeXLarge: 1.25,
    fontSizeLarge: 1.125,
    fontSizeMedium: 1,
    fontSizeSmall: 0.875,
    fontSizeXSmall: 0.75,
  };

  static fontLeading = {
    none: 1,
    tight: 1.3,
    normal: 1.5,
    loose: 1.8,
  };

  static fontTracking = {
    tight: -0.05,
    normal: 0,
    wide: 0.05,
  };

  // font
  static baseFontSize = 16;

  static baseLineHeight = 1.5;

  // border

  static borderWidth = 1;
  static borderColor = Variables.gray300;

  static borderRadiusSmall = Variables.baseFontSize * 0.125;
  static borderRadius = Variables.baseFontSize * 0.25;
  static borderRadiusMedium = Variables.baseFontSize * 0.375;
  static borderRadiusLarge = Variables.baseFontSize * 0.5;
  static borderRadiusXLarge = Variables.baseFontSize * 0.75;
  static borderRadius2Xlarge = Variables.baseFontSize * 1;
  static borderRadius3Xlarge = Variables.baseFontSize * 1.5;
  static borderRadiusFull = Variables.baseFontSize * 1000;

  static roundedCircle = Variables.baseFontSize * 1000;
  static roundedPill = Variables.baseFontSize * 1000;
}
