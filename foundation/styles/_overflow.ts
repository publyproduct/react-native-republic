import { StyleSheet } from 'react-native';

const overflow = StyleSheet.create({
  visible: { overflow: 'visible' },
  hidden: { overflow: 'hidden' },
  scroll: { overflow: 'scroll' },
});

export default overflow;
