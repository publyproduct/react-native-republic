import TextBold from './TextBold';
import TextRegular from './TextRegular';

export { TextBold, TextRegular };
