/* eslint-disable react/jsx-props-no-spreading */
import { font } from '@republic/foundation/styles';
import React from 'react';
import { Text, TextProps } from 'react-native';

export default function TextExtraLight(props: React.PropsWithChildren<TextProps>) {
  return (
    <Text {...props} style={[
      font.weight.w200,
      props.style
    ]}>
      {props.children}
    </Text>
  );
}
