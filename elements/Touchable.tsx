import React from 'react';
import { TouchableOpacity, TouchableOpacityProps } from 'react-native';

export default function Touchable(props: React.PropsWithChildren<TouchableOpacityProps>) {
  const { children, activeOpacity = 0.6, ...restProps } = props;

  return (
    <TouchableOpacity activeOpacity={activeOpacity} {...restProps}>
      {children}
    </TouchableOpacity>
  );
}
