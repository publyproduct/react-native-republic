import Badge from './Badge';
import Tag from './Tag';
import Touchable from './Touchable';
import { TextBold, TextRegular, SelectableTextRegular } from './text';

export { Badge, Tag, TextBold, TextRegular, SelectableTextRegular, Touchable };
